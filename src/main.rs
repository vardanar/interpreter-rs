pub mod fml;
mod interpreter;
pub mod parser;

use fml::TopLevelParser;
use interpreter::Interpreter;

use std::{
    env,
    fs::File,
    io::{self, Read},
};

fn main() {
    let args: Vec<_> = env::args().collect();

    let mut r: Box<dyn Read> = if args.len() != 3 || args[1] != "run" {
        Box::new(io::stdin())
    } else {
        let file = File::open(&args[2]).unwrap();
        Box::new(file)
    };
    let mut s = String::new();
    r.read_to_string(&mut s).expect("read to string failed");

    let ast = TopLevelParser::new()
        .parse(&s)
        .expect("Error reading input");

    Interpreter::new(io::stdout()).run(&ast).unwrap();
    ()
}
