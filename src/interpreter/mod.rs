use Val::Num;

use crate::parser::AST::*;
use crate::parser::{Identifier, AST};
use std::fmt;
use std::{collections::HashMap, io};

type RefVal = i32;
type RefObj = i32;
type RefArr = i32;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Val {
    Num(i32),
    Boolean(bool),
    Null,
    Object(RefObj),
    Array(RefArr),
}

impl fmt::Display for Val {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Num(v) => write!(f, "{}", v),
            Val::Boolean(v) => write!(f, "{}", v),
            Val::Null => write!(f, "null"),
            Val::Array(v) => write!(f, "{}", v),
            Val::Object(v) => write!(f, "{}", v),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Fun {
    name: Identifier,
    parameters: Vec<Identifier>,
    body: Box<AST>,
}

impl fmt::Display for Fun {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "{}", self.name)
    }
}

#[derive(Debug, Clone)]
pub struct Obj {
    parent: Val,
    mem_vars: HashMap<String, Val>,
    mem_methods: FunEnv,
}

impl fmt::Display for Obj {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut res = self
            .mem_vars
            .iter()
            .map(|(k, v)| format!("{}={}", k, v))
            .collect::<Vec<String>>();
        res.sort();

        write!(f, "object({})", res.join(", "))
    }
}

#[derive(Debug, Clone)]
pub struct Arr {
    data: Vec<Val>,
}

impl fmt::Display for Arr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(
            f,
            "[{}]",
            self.data
                .iter()
                .map(|v| format!("{}", v))
                .collect::<Vec<String>>()
                .join(", ")
        )
    }
}

pub(crate) type Env = HashMap<String, RefVal>;

type Store = HashMap<RefVal, Val>;

type FunEnv = HashMap<String, Fun>;

type ObjEnv = HashMap<RefObj, Obj>;

type ArrEnv = HashMap<RefArr, Arr>;

type CallStack = Vec<Env>;

#[derive(Debug)]
pub struct Interpreter<W: io::Write> {
    env: Env,
    call_stack: CallStack,
    store: Store,
    fun_env: FunEnv,
    obj_env: ObjEnv,
    arr_env: ArrEnv,
    arr_cnt: RefArr,
    ref_cnt: RefVal,
    obj_cnt: RefObj,
    wr: W,
}

impl<W: io::Write> Interpreter<W> {
    pub fn new(wr: W) -> Interpreter<W> {
        Interpreter {
            env: HashMap::new(),
            call_stack: CallStack::new(),
            store: HashMap::new(),
            fun_env: HashMap::new(),
            obj_env: HashMap::new(),
            arr_env: HashMap::new(),
            arr_cnt: 0,
            ref_cnt: 0,
            obj_cnt: 0,
            wr,
        }
    }

    pub fn get_next_ref(&mut self) -> RefVal {
        self.ref_cnt += 1;
        self.ref_cnt
    }

    pub fn get_next_obj_ref(&mut self) -> RefObj {
        self.obj_cnt += 1;
        self.obj_cnt
    }

    pub fn get_next_arr_ref(&mut self) -> RefObj {
        self.arr_cnt += 1;
        self.arr_cnt
    }

    pub fn run(&mut self, ast: &AST) -> Result<Val, String> {
        match ast {
            Integer(v) => Ok(Num(v.clone())),
            Boolean(v) => Ok(Val::Boolean(v.clone())),
            Null => Ok(Val::Null),
            Print { format, arguments } => {
                let args = arguments
                    .iter()
                    .map(|x| self.run(&*x))
                    .collect::<Result<Vec<_>, String>>()?;
                self.print(format, args)?;
                Ok(Val::Null)
            }
            Top(stms) => stms.iter().try_fold(Val::Null, |_, stmt| self.run(stmt)),
            Variable { name, value } => {
                let val = self.run(value)?;
                let rv = self.insert_id(name);
                self.store.insert(rv, val);
                Ok(val)
            }
            //TODO: does not handle unknown variables
            AssignVariable { name, value } => {
                let val = self.run(value)?;
                let ref_val = self.get_ref_val(&name)?;
                self.store.insert(ref_val, val);
                Ok(val)
            }
            AccessVariable { name } => self.get_val(name),
            Block(stms) => {
                let new_cs = match self.call_stack.last() {
                    Some(cs) => cs.clone(),
                    None => HashMap::new(),
                };
                self.call_stack.push(new_cs);
                let val = stms.iter().try_fold(Val::Null, |_, stmt| self.run(stmt))?;
                self.call_stack.pop();
                Ok(val)
            }
            Function {
                name,
                parameters,
                body,
            } => {
                if self.fun_env.contains_key(&name.0) {
                    Err(format!("Function '{}' already defined", name.0))
                } else {
                    let new_fun = Fun {
                        name: name.clone(),
                        parameters: parameters.clone(),
                        body: body.clone(),
                    };
                    self.fun_env.insert(name.0.clone(), new_fun);
                    Ok(Val::Null)
                }
            }
            CallFunction { name, arguments } => {
                let fun = match self.fun_env.get(&name.0) {
                    Some(f) => f.clone(),
                    None => return Err(format!("Undefined function '{}'", name.0)),
                };
                self.run_fun(fun, arguments, None)
            }
            Conditional {
                condition,
                consequent,
                alternative,
            } => match self.run(condition)? {
                Val::Boolean(false) | Val::Null => self.run(alternative),
                _ => self.run(consequent),
            },
            Loop { condition, body } => loop {
                match self.run(condition)? {
                    Val::Boolean(false) | Val::Null => break Ok(Val::Null),
                    _ => self.run(body)?,
                };
            },
            CallMethod {
                object,
                name,
                arguments,
            } => {
                let val = self.run(&*object)?;
                self.method_dispatch(val, name, arguments)
            }
            Object { extends, members } => {
                let parent = self.run(extends)?;
                let (mem_vars, mem_methods) = members.iter().try_fold(
                    (HashMap::new(), HashMap::new()),
                    |mut acc, x| match *x.clone() {
                        Variable { name, value } => {
                            if acc.0.contains_key(&name.0) {
                                Err(format!("Variable {} already defined in object", &name.0))
                            } else {
                                let val = self.run(&*value)?;
                                acc.0.insert(name.0.clone(), val);
                                Ok(acc)
                            }
                        }
                        Function {
                            name,
                            parameters,
                            body,
                        } => {
                            if acc.1.contains_key(&name.0) {
                                Err(format!("Method {} already defined in object", &name.0))
                            } else {
                                let fun = Fun {
                                    name: name.clone(),
                                    parameters: parameters.clone(),
                                    body: body.clone(),
                                };
                                acc.1.insert(name.0.clone(), fun);
                                Ok(acc)
                            }
                        }
                        _ => Err(format!(
                            "Object can contain only contain member fields or methods"
                        )),
                    },
                )?;
                let obj_ref = self.get_next_obj_ref();
                let obj = Obj {
                    parent,
                    mem_vars,
                    mem_methods,
                };
                self.obj_env.insert(obj_ref, obj);
                Ok(Val::Object(obj_ref))
            }
            AccessField { object, field } => match self.run(object)? {
                Val::Object(ref_obj) => {
                    let obj = self.obj_env.get(&ref_obj).unwrap();
                    match obj.mem_vars.get(&field.0) {
                        Some(v) => Ok(*v),
                        None => Err(format!("Unknown object field {}", field.0)),
                    }
                }
                _ => Err(format!("Can't access field of non-object type")),
            },
            AssignField {
                object,
                field,
                value,
            } => {
                let val = self.run(value)?;
                let obj = match self.run(object)? {
                    Val::Object(ref_obj) => self.obj_env.get_mut(&ref_obj).unwrap(),
                    _ => return Err(format!("Can't access field of non-object type")),
                };
                match obj.mem_vars.contains_key(&field.0) {
                    true => {
                        obj.mem_vars.insert(field.0.clone(), val);
                        Ok(val)
                    }
                    false => Err(format!("Unknown object field {}", field.0)),
                }
            }
            Array { size, value } => {
                let size = match self.run(size)? {
                    Num(v) => v,
                    r => return Err(format!("Expected number got {}", r)),
                };
                let mut data = vec![Val::Null; size as usize];
                for i in 0..size {
                    let cs = match self.call_stack.last() {
                        Some(cs) => cs.clone(),
                        None => HashMap::new(),
                    };
                    self.call_stack.push(cs);
                    let result = self.run(value)?;
                    self.call_stack.pop();
                    data[i as usize] = result;
                }
                let id = self.get_next_arr_ref();
                self.arr_env.insert(id, Arr { data });
                Ok(Val::Array(id))
            }
            AccessArray { array, index } => {
                let arr_val = self.run(array)?;
                match arr_val {
                    Val::Object(_) => self.method_dispatch(
                        arr_val,
                        &Identifier("get".to_owned()),
                        &vec![index.clone()],
                    ),
                    Val::Array(_) => {
                        let i = match self.run(index)? {
                            Val::Num(v) => v,
                            _ => return Err(format!("expected number")),
                        };
                        let arr = self.find_arr_parrent(arr_val)?;
                        if i as usize > arr.data.len() {
                            Err(format!("array {} out of bounds", arr_val))
                        } else {
                            Ok(arr.data[i as usize])
                        }
                    }
                    v => Err(format!("unexpected {:?}", v)),
                }
            }
            AssignArray {
                array,
                index,
                value,
            } => {
                let arr_val = self.run(array)?;
                let val = self.run(value)?;
                match arr_val {
                    Val::Object(_) => self.method_dispatch(
                        arr_val,
                        &Identifier("set".to_owned()),
                        &vec![index.clone(), value.clone()],
                    ),
                    Val::Array(_) => {
                        let i = match self.run(index)? {
                            Val::Num(v) => v,
                            _ => return Err(format!("expected number")),
                        };
                        let arr = self.find_arr_parrent(arr_val)?;
                        if i as usize > arr.data.len() {
                            Err(format!("array {} out of bounds", arr_val))
                        } else {
                            arr.data[i as usize] = val;
                            Ok(Val::Null)
                        }
                    }
                    v => Err(format!("unexpected {:?}", v)),
                }
            }
        }
    }

    fn find_arr_parrent(&mut self, a: Val) -> Result<&mut Arr, String> {
        match a {
            Val::Array(v) => self
                .arr_env
                .get_mut(&v)
                .ok_or(format!("undeclared array {}", a)),
            Val::Object(o) => {
                let obj = self
                    .obj_env
                    .get(&o)
                    .ok_or(format!("undeclared array {}", a))?
                    .clone();
                self.find_arr_parrent(obj.parent)
            }
            _ => Err(format!("undeclared array {}", a)),
        }
    }

    fn run_fun(
        &mut self,
        fun: Fun,
        arguments: &Vec<Box<AST>>,
        caller_ref: Option<RefObj>,
    ) -> Result<Val, String> {
        if fun.parameters.len() != arguments.len() {
            return Err(format!(
                "Incorrect number of arguments for function '{}'",
                fun.name.0
            ));
        }
        let mut new_cs = fun
            .parameters
            .iter()
            .zip(arguments.iter())
            .try_fold::<_, _, Result<_, String>>(Env::new(), |mut acc, x| {
                let val = self.run(x.1)?;
                let rv = self.get_next_ref();
                acc.insert(x.0 .0.clone(), rv);
                self.store.insert(rv, val);
                Ok(acc)
            })?;
        match caller_ref {
            Some(x) => {
                let rv = self.get_next_ref();
                new_cs.insert("this".to_string(), rv);
                self.store.insert(rv, Val::Object(x));
            }
            None => (),
        }
        self.call_stack.push(new_cs);
        let val = self.run(&*fun.body);
        self.call_stack.pop();
        val
    }

    fn print(&mut self, format: &String, args: Vec<Val>) -> Result<(), String> {
        if args.len() != (format.matches("~").count() - format.matches("\\~").count()) {
            return Err(
                "Number of arguments doesn't match number of placeholders in print".to_owned(),
            );
        }
        format.chars().try_fold((false, 0), |acc, x| match x {
            '~' if acc.0 => {
                write!(self.wr, "~").unwrap();
                Ok((false, acc.1))
            }
            '~' => {
                self.print_val(args.get(acc.1).unwrap().clone());
                Ok((false, acc.1 + 1))
            }
            'n' if acc.0 => {
                write!(self.wr, "\n").unwrap();
                Ok((false, acc.1))
            }
            't' if acc.0 => {
                write!(self.wr, "\t").unwrap();
                Ok((false, acc.1))
            }
            'r' if acc.0 => {
                write!(self.wr, "\r").unwrap();
                Ok((false, acc.1))
            }
            '"' if acc.0 => {
                write!(self.wr, "\"").unwrap();
                Ok((false, acc.1))
            }
            '\\' if !acc.0 => Ok((true, acc.1)),
            '\\' => {
                write!(self.wr, "\\").unwrap();
                Ok((false, acc.1))
            }
            _ if acc.0 => Err("Unknown escape sequence in print"),
            _ => {
                write!(self.wr, "{}", x).unwrap();
                Ok((false, acc.1))
            }
        })?;
        Ok(())
    }

    fn print_val(&mut self, arg: Val) {
        match arg {
            Val::Object(k) => write!(self.wr, "{}", self.obj_env.get(&k).unwrap()).unwrap(),
            Val::Array(k) => write!(self.wr, "{}", self.arr_env.get(&k).unwrap()).unwrap(),
            _ => write!(self.wr, "{}", arg).unwrap(),
        };
    }

    fn method_dispatch(
        &mut self,
        caller: Val,
        name: &Identifier,
        arguments: &Vec<Box<AST>>,
    ) -> Result<Val, String> {
        match caller {
            Val::Num(n) => {
                let val2 = self.run(&*arguments[0])?;
                //TODO: we need function that returns top-most parent of val2 (if val2 is not an object, return itself), which is a simple type (null|int|bool)
                //      match the result in the IF below instead of val2
                if let Num(n2) = val2 {
                    Ok(match name.as_str() {
                        "+" => Num(n + n2),
                        "-" => Num(n - n2),
                        "/" => Num(n / n2),
                        "*" => Num(n * n2),
                        "%" => Num(n % n2),
                        "==" => Val::Boolean(n == n2),
                        "!=" => Val::Boolean(n != n2),
                        "<" => Val::Boolean(n < n2),
                        ">" => Val::Boolean(n > n2),
                        ">=" => Val::Boolean(n >= n2),
                        "<=" => Val::Boolean(n <= n2),
                        v => return Err(format!("Unknown method: {}", v)),
                    })
                } else {
                    match name.as_str() {
                        "==" => Ok(Val::Boolean(false)),
                        "!=" => Ok(Val::Boolean(true)),
                        _ => Err(format!("Expected a number got {:?}", val2)),
                    }
                }
            }
            Val::Boolean(b) => {
                let v2 = self.run(&*arguments[0])?;
                //TODO: see note for Val::Num
                if let Val::Boolean(b2) = v2 {
                    Ok(Val::Boolean(match name.as_str() {
                        "|" => b || b2,
                        "&" => b && b2,
                        "==" => b == b2,
                        "!=" => b != b2,
                        v => return Err(format!("Unknown method: {}", v)),
                    }))
                } else {
                    match name.as_str() {
                        "==" => Ok(Val::Boolean(false)),
                        "!=" => Ok(Val::Boolean(true)),
                        _ => Err(format!("Expected a bool got {:?}", v2)),
                    }
                }
            }
            Val::Null => {
                if name.as_str() != "==" && name.as_str() != "!=" {
                    return Err(format!("Unknown method: {}", name));
                }
                if let Val::Null = self.run(&*arguments[0])? {
                    Ok(Val::Boolean(name.as_str() == "=="))
                } else {
                    Ok(Val::Boolean(name.as_str() == "!="))
                }
            }
            Val::Object(ref_obj) => {
                let obj = self.obj_env.get(&ref_obj).map(Clone::clone).unwrap();
                match obj.mem_methods.get(&name.0).map(Clone::clone) {
                    Some(f) => self.run_fun(f, arguments, Some(ref_obj)),
                    None => self.method_dispatch(obj.parent, name, arguments),
                }
            }
            Val::Array(ref_arr) => {
                let val2 = self.run(&*arguments[0])?;
                if let Val::Array(ref_arr2) = val2 {
                    Ok(Val::Boolean(match name.as_str() {
                        "==" => ref_arr == ref_arr2,
                        "!=" => ref_arr != ref_arr2,
                        v => return Err(format!("Unknown method: {}", v)),
                    }))
                } else {
                    Err(format!("Expected a bool found: {}", val2))
                }
            }
        }
    }

    fn get_ref_val(&self, id: &Identifier) -> Result<RefVal, String> {
        let res = self
            .call_stack
            .last()
            .map(|x| x.get(&id.0))
            .flatten()
            .or(self.env.get(&id.0));
        match res {
            Some(r) => Ok(*r),
            None => Err(format!("Undefined variable {}", &id.0)),
        }
    }

    fn get_val(&self, id: &Identifier) -> Result<Val, String> {
        let ref_val = self.get_ref_val(id)?;
        Ok(self.store.get(&ref_val).unwrap().clone())
    }

    fn insert_id(&mut self, id: &Identifier) -> RefVal {
        let ref_val = self.get_next_ref();
        if self.call_stack.is_empty() {
            //println!("Inserting var {} into global env", id.0);
            self.env.insert(id.0.clone(), ref_val);
        } else {
            //println!("Inserting var {} into local env", id.0);
            self.call_stack
                .last_mut()
                .unwrap()
                .insert(id.0.clone(), ref_val);
        }
        ref_val
    }
}

#[cfg(test)]
mod tests {
    use super::Interpreter;
    use super::Val;

    #[test]
    fn operators() {
        use crate::parser::AST::*;
        use crate::parser::*;

        let mut out = Vec::new();
        let res = Interpreter::new(&mut out)
            .run(&Top(vec![Box::new(CallMethod {
                object: Box::new(Integer(1)),
                name: Identifier("+".to_owned()),
                arguments: vec![Box::new(Integer(1))],
            })]))
            .unwrap();
        assert_eq!(res, Val::Num(2))
    }
}
